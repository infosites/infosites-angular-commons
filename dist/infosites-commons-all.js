var infositesCommons = angular.module ( 'infosites-commons', [ 'ngMessages', 'infosites-commons-templates' ] );

infositesCommons.factory ( 'flash', function () {
	return { message: '' };
} );

infositesCommons.directive ( 'istFlashMessage', [ 'flash', function ( flash ) {
	return {
		restrict: 'AE',
		replace: true,
		scope: { key: '@' },
		templateUrl: 'templates/ist-flash-message/ist-flash-message.html',
		link: function ( scope ) {
			if ( scope.key ) {
				scope.$parent.$watch ( scope.key, function ( message ) {
					scope.message = message;
				} );
			} else {
				scope.message = flash.message;
				flash.message = '';
			}
		}
	};
} ] );

infositesCommons.filter ( 'bytes', [ function ( ) {
	return function( bytes, precision ) {
		if ( isNaN( parseFloat( bytes ) ) || !isFinite( bytes ) ) return '-';
		if ( typeof precision === 'undefined' ) precision = 1;
		var units = [ 'bytes', 'kB', 'MB', 'GB', 'TB', 'PB' ],
			number = Math.floor( Math.log( bytes ) / Math.log( 1024 ) );
		return ( bytes / Math.pow( 1024, Math.floor( number ) ) ).toFixed( precision ) +  ' ' + units[ number ];
	};
} ] );

infositesCommons.directive ( 'istLabel', [ function ( ) {
	return {
		restrict: 'AE',
		replace: true,
		scope: { text: '@', type: '@', index: '=' },
		templateUrl: 'templates/ist-label/ist-label.html',
		link: function ( scope ) {
			if ( scope.index != null ) scope.type = ( scope.index == 0 ) ? 'default' : ( scope.index < 0 ) ? 'danger' : 'success';
		}
	};
} ] );

infositesCommons.directive ( 'istBoolean', [ function ( ) {
	return {
		restrict: 'AE',
		replace: true,
		scope: { value: '=' },
		templateUrl: 'templates/ist-boolean/ist-boolean.html'
	};
} ] );

infositesCommons.directive ( 'istErrorMessage', [ function () {
	return {
		restrict: 'AE',
		replace: true,
		scope: { key: '@' },
		templateUrl: 'templates/ist-error-message/ist-error-message.html',
		link: function ( scope ) {
			if ( !scope.key ) scope.key = 'errorMessage';
			scope.$parent.$watch ( scope.key, function ( errorMessage ) {
				scope.errorMessage = errorMessage;
			} );
		}
	};
} ] );

infositesCommons.directive ( 'istFormFieldCompareTo', [ function () {
	return {
		require: 'ngModel',
		scope: {
			otherModelValue: '=istFormFieldCompareTo'
		},
		link: function ( scope, element, attributes, ngModel ) {
			ngModel.$validators.compareTo = function ( modelValue ) {
				return modelValue == scope.otherModelValue;
			};

			scope.$watch ( 'otherModelValue', function () {
				ngModel.$validate ();
			} );
		}
	};
} ] );

infositesCommons.directive ( 'istFormActions', [ function () {
	return {
		restrict: 'AE',
		replace: true,
		transclude: true,
		scope: false,
		templateUrl: 'templates/ist-form-actions/ist-form-actions.html'
	};
} ] );

infositesCommons.directive ( 'istFormField', [ '$timeout', function ( $timeout ) {
	return {
		restrict: 'AE',
		replace: true,
		transclude: true,
		require: '^form',
		scope: {
			label: '@', type: '@', help: '=', required: '@', errors: '=', id: '@'
		},
		templateUrl: function ( elem, attrs ) {
			if ( attrs.type ) {
				return 'templates/ist-form-field/ist-form-field-' + attrs.type + '.html';
			} else {
				return 'templates/ist-form-field/ist-form-field.html';
			}
		},
		link: function ( scope, element, attrs, controller ) {
			var id = null;

			$timeout( function() {
				id = scope.id ? scope.id : $ ( "[input='radio'], [input='hidden'], .form-control", element ).attr ( 'id' );
				scope.for = id;

				scope.helpList = [];
				if ( scope.help )
					if ( scope.help.constructor === Array )
						scope.helpList = scope.help;
					else
						scope.helpList.push( scope.help );

				scope.field = controller[ id ];
			} );

			scope.$on ( 'show-form-errors', function ( ev, form ) {
				if ( form && form[ id ] ) scope.field = form[ id ];
				scope.field.$setDirty ();
			} );

			scope.$parent.$watch ( 'errors', function ( errors ) {
				if ( errors != null ) {
					scope.isError = errors[ id ] != null;
					scope.validationMessage = errors[ id ];
				}
			} );
		}
	};
} ] );

infositesCommons.directive ( 'istDateTime', [ '$log', 'dateFilter', function ( $log, dateFilter ) {
	return {
		replace: true,
		require: '?ngModel',
		scope: { name: '@', format: '=' },
		templateUrl: 'templates/ist-date-time/ist-date-time.html',
		link: function ( $scope, element, $attrs, ngModel ) {
			var logger = $log.getInstance ( 'istDateTime' );
			logger.debug ( ngModel );
			ngModel.$formatters.push ( function ( modelValue ) {
				return moment( modelValue ).format( $scope.format );
			} );

			ngModel.$parsers.push ( function ( viewValue ) {
				return moment ( viewValue, $scope.format, true ).toDate();
			} );
		}
	}
} ] );

infositesCommons.directive('istFile', [function () {
    return {
        require: "ngModel",
        restrict: 'A',
        link: function ($scope, el, attrs, ngModel) {
            el.bind('change', function (event) {
                ngModel.$setViewValue(event.target.files[0]);
                $scope.$apply();
            });

            $scope.$watch(function () {
                return ngModel.$viewValue;
            }, function (value) {
                if (!value) {
                    el.val("");
                }
            });
        }
    };
}]);

infositesCommons.directive( 'istOnPressEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval( attrs.istOnPressEnter );
                } );

                event.preventDefault();
            }
        } );
    };
} );

infositesCommons.directive ( 'istChange', [ '$parse', function ( $parse ) {
	return {
		restrict: 'A',
		link: function ( $scope, element, attrs ) {
			var attrHandler = $parse ( attrs[ 'istChange' ] );

			var handler = function ( e ) {
				$scope.$apply ( function () {
					attrHandler ( $scope, { $event: e, files: e.target.files } );
				} );
			};

			element[ 0 ].addEventListener ( 'change', handler, false );
		}
	};
} ] );

infositesCommons.directive ( 'istResetServerError', [ '$parse', function ( $parse ) {
	return {
		restrict: 'A',
		require: '?ngModel',
		link: function (scope, element, attrs, controller) {
			element.on('input change', function () {
				scope.$apply(function () {
					delete controller.$error.message;
					controller.$setValidity('server', true);
				});
			});
		}
	};
} ] );

infositesCommons.directive ( 'istAutoSubmitForm', [ '$log', '$sce', '$timeout', function ( $log, $sce, $timeout ) {
	return {
		replace: true,
		scope: {},
		templateUrl: 'templates/ist-auto-submit-form/ist-auto-submit-form.html',
		link: function ( $scope, element, $attrs ) {
			$scope.$on ( $attrs[ 'event' ], function ( event, data ) {
				data.redirectUrl = $sce.trustAsResourceUrl ( data.redirectUrl );
				$scope.formData = data;
				$log.debug ( 'redirecting now!' );
				$log.debug ( data );
				$timeout ( function () {
					element.submit ();
				} )
			} )
		}
	}
} ] );

infositesCommons.directive ( 'istPagination', [ '$location', function ( $location ) {
	return {
		restrict: 'AE',
		replace: true,
		require: 'ngModel',
		scope: { ngModel: '=' },
		templateUrl: 'templates/ist-pagination/ist-pagination.html',
		link: function ( scope, element, attrs, ctrl ) {
			scope.$watch ( 'ngModel', function ( newValue ) {
				if ( !newValue ) return;
				scope.count = newValue.count;
				scope.first = (newValue.page * newValue.pageSize) + 1 - newValue.pageSize;
				scope.last = newValue.page * newValue.pageSize;
				scope.last = (scope.last > newValue.count) ? newValue.count : scope.last;
				scope.pageCount = Math.ceil ( newValue.count / newValue.pageSize );

				scope.firstPage = newValue.page - 5;
				scope.firstPage = (scope.firstPage < 1) ? 1 : scope.firstPage;

				scope.lastPage = scope.firstPage + 9;
				scope.lastPage = (scope.lastPage > scope.pageCount) ? scope.pageCount : scope.lastPage;

				scope.firstPage = (scope.lastPage - scope.firstPage < 9) ? scope.lastPage - 9 : scope.firstPage;
				scope.firstPage = (scope.firstPage < 1) ? 1 : scope.firstPage;

				scope.pages = [];
				for ( var i = scope.firstPage; i <= scope.lastPage; i++ ) {
					scope.pages.push ( i );
				}
			} );

			scope.goToPage = function ( page ) {
				var path = $location.path ();
				var search = $.extend ( true, {}, $location.search () );
				search.page = page;
				return '#' + path + '?' + $.param ( search );
			};
		}
	};
} ] );

infositesCommons.directive ( 'istGoTo', [ '$location', function ( $location ) {
	return {
		restrict: 'A',
		link: function ( scope, element, attr ) {
			element.attr ( 'style', 'cursor:pointer' );
			element.on ( 'click', function () {
				$location.path ( attr.istGoTo );
				scope.$apply ();
			} );
		}
	};
} ] );

infositesCommons.directive('istStopEvent', function () {
    return {
        restrict: 'A',
        link: function ( scope, element, attr ) {
            if ( attr && attr.istStopEvent )
                element.bind(attr.istStopEvent, function (e) {
                    e.stopPropagation();
                });
        }
    };
});

infositesCommons.factory ( 'errorHandlingService', [ '$log', function ( log ) {
	var result = {};

	result.handleErrorFromModelState = function ( modelState ) {
		var logger = log.getInstance ( 'errorHandlingService.handleErrorFromModelState' );
		logger.error ( modelState );
		var errors = {};
		for ( var key in modelState ) {
			for ( var i = 0; i < modelState[ key ].length; i++ ) {
				errors[ key ] = modelState[ key ][ i ];
			}
		}
		return errors;
	};

	result.handleErrorFromSequelize = function ( err ) {
		var logger = log.getInstance ( 'errorHandlingService.handleErrorFromSequelize' );
		logger.error ( err );
		var errors = {};
		for ( var index in err.errors ) {
			var error = err.errors[ index ];
			errors[ error.path ] = error.type.replace ( ' Violation', '' ).replace ( ' violation', '' );
		}
		return errors;
	};

	result.handleErrorFromGrails = function ( err ) {
		var logger = log.getInstance ( 'errorHandlingService.handleErrorFromGrails' );
		logger.error ( err );
		var errors = {};
		for ( var index in err.errors ) {
			var error = err.errors[ index ];
			errors[ error.field ] = error.message;
		}
		return errors;
	};

	result.applyToForm = function ( form, errors ) {
		var logger = log.getInstance ( 'errorHandlingService.applyToForm' );

		for ( var name in form ) {
			if ( name.substring ( 0, 1 ) != '$' ) {
				var field = form[ name ];

				if ( field ) {
					if ( errors[ name ] ) {
						field.$setValidity ( name, false );
						field.$error.server = true;
						field.$error.message = errors[ name ];
					} else {
						field.$setValidity ( name, true );
						field.$error.server = false;
						field.$error.message = null;
					}
				}
			}
		}
	};
	
	result.applyToFields = function( fields, errors ) {
        for ( var i in fields ) {
            var field = fields[ i ];
            if ( field.key && errors[ field.key ] ) {
                field.formControl.$setValidity( 'server', false );
                field.formControl.$error.message = errors[ field.key ];
            }
            if ( field.fieldGroup != null ) {
                result.applyToFields( field.fieldGroup, errors );
            }
        }
	};

	result.resetFormErrors = function ( form ) {
		form.$setPristine ();
		form.$setUntouched ();

		for ( var name in form ) {
			if ( name.substring ( 0, 1 ) != '$' ) {
				var field = form[ name ];
				field.$setValidity ( name, true );
			}
		}
	};

	return result;
} ] );

infositesCommons.factory ( 'repositoryService', [ 'ENV', '$resource', '$q', function ( ENV, $resource, $q ) {
	var _create = function ( entityName ) {
		var _resource = $resource ( ENV.serviceBase + entityName + '/:id', null, {
			'list': { method: 'GET' },
			'update': { method: 'PUT' }
		} );

		var _list = function ( data ) {
			var deferred = $q.defer ();
			var list = _resource.list ( data, deferred.resolve, deferred.reject );
			return deferred.promise;
		};

		var _get = function ( id ) {
			var deferred = $q.defer ();
			var entity = _resource.get ( { id: id }, deferred.resolve, deferred.reject );
			return deferred.promise;
		};

		var _delete = function ( id ) {
			var deferred = $q.defer ();
			_resource.delete ( { id: id }, deferred.resolve, deferred.reject );
			return deferred.promise;
		};

		var _save = function ( entity ) {
			var deferred = $q.defer ();
			if ( entity.id ) {
				_resource.update ( { id: entity.id }, entity, deferred.resolve, function ( response ) {
					deferred.reject ( _errorHandler ( response ), response );
				} );
			} else {
				_resource.save ( null, entity, function ( response ) {
					entity.id = response.id;
					deferred.resolve ( entity );
				}, function ( response ) {
					deferred.reject ( _errorHandler ( response ), response );
				} );
			}
			return deferred.promise;
		};

		var _errorHandler = function ( response ) {
			var errors = {};
			for ( var key in response.data.ModelState ) {
				for ( var i = 0; i < response.data.ModelState[ key ].length; i++ ) {
					errors[ key ] = response.data.ModelState[ key ][ i ];
				}
			}
			return errors;
		};

		return {
			list: _list,
			get: _get,
			delete: _delete,
			save: _save,
			_resource: _resource
		};
	};

	return {
		create: _create
	};
} ] );

angular.module("infosites-commons-templates", []).run(["$templateCache", function($templateCache) {$templateCache.put("templates/ist-auto-submit-form/ist-auto-submit-form.html","<form action=\"{{formData.redirectUrl}}\" method=\"{{formData.redirectMethod}}\">\r\n	<div ng-repeat=\"(key,val) in formData.redirectData\">\r\n		<input type=\"hidden\" name=\"{{key}}\" value=\"{{val}}\" />\r\n	</div>\r\n</form>");
$templateCache.put("templates/ist-boolean/ist-boolean.html","<i class=\"fa\" ng-class=\"{ \'fa-check\': value, \'fa-times\': !value }\"></i>");
$templateCache.put("templates/ist-date-time/ist-date-time.html","<input type=\"text\" id=\"{{name}}\" name=\"{{name}}\" class=\"form-control\" ui-mask=\"99/99/9999 99:99\" />");
$templateCache.put("templates/ist-error-message/ist-error-message.html","<div class=\"alert alert-danger\" role=\"alert\" ng-show=\"errorMessage\">\r\n	<i class=\"fa fa-exclamation-triangle\"></i> {{errorMessage}}\r\n</div>");
$templateCache.put("templates/ist-flash-message/ist-flash-message.html","<div class=\"alert alert-success\" role=\"alert\" ng-show=\"message\">\r\n	<i class=\"fa fa-check-circle\"></i> {{message}}\r\n</div>");
$templateCache.put("templates/ist-form-actions/ist-form-actions.html","<div class=\"form-group\">\r\n	<div class=\"col-sm-offset-3 col-sm-9\">\r\n		<span ng-transclude></span>\r\n	</div>\r\n</div>\r\n");
$templateCache.put("templates/ist-form-field/ist-form-field-checkbox.html","<div class=\"form-group\" ng-class=\"{ \'has-error\': isError }\">\r\n	<div class=\"col-sm-offset-3 col-sm-9\">\r\n		<div class=\"checkbox\">\r\n			<label><span ng-transclude></span> {{label}}</label>\r\n		</div>\r\n        <span class=\"help-block\" ng-show=\"field.$dirty && field.$invalid\" ng-messages=\"field.$error\" role=\"alert\">\r\n            <i class=\"fa fa-exclamation-circle\"></i>\r\n\r\n            <span ng-message=\"required\">Obrigatório</span>\r\n            <span ng-message=\"email\">Inválido</span>\r\n            <span ng-message=\"compareTo\">Senhas não conferem</span>\r\n            <span ng-message=\"maxlength\">Tamanho máximo do campo excedido.</span>\r\n            <span ng-message=\"server\">{{field.$error.message}}</span>\r\n        </span>\r\n		<span class=\"help-block\" data-ng-show=\"help\" ng-repeat=\"h in helpList\"><i class=\"fa fa-lightbulb-o\"></i> {{h}}</span>\r\n	</div>\r\n</div>");
$templateCache.put("templates/ist-form-field/ist-form-field.html","<div class=\"form-group\" ng-class=\"{ \'has-error\': field.$dirty && field.$invalid }\">\r\n	<label class=\"col-sm-3 control-label\" for=\"{{for}}\">\r\n		{{label}}\r\n		<strong title=\"Obrigatório\" data-ng-show=\"required == \'true\'\">*</strong>\r\n	</label>\r\n	<div class=\"col-sm-9\">\r\n		<span ng-transclude></span>\r\n        <span class=\"help-block\" ng-show=\"field.$dirty && field.$invalid\" ng-messages=\"field.$error\" role=\"alert\">\r\n            <i class=\"fa fa-exclamation-circle\"></i>\r\n\r\n            <span ng-message=\"required\">Obrigatório</span>\r\n            <span ng-message=\"email\">Inválido</span>\r\n            <span ng-message=\"mask\">Inválido</span>\r\n            <span ng-message=\"cpf\">Inválido</span>\r\n            <span ng-message=\"compareTo\">Senhas não conferem</span>\r\n            <span ng-message=\"maxlength\">Tamanho máximo do campo excedido.</span>\r\n            <span ng-message=\"server\">{{ field.$error.message }}</span>\r\n        </span>\r\n		<span class=\"help-block\" data-ng-show=\"help\" ng-repeat=\"h in helpList\"><i class=\"fa fa-lightbulb-o\"></i> {{h}}</span>\r\n	</div>\r\n</div>");
$templateCache.put("templates/ist-label/ist-label.html","<span class=\"label\" ng-class=\"{ \'label-default\': type == \'default\', \'label-success\': type == \'success\', \'label-danger\': type == \'danger\' }\">{{ text }}</span>");
$templateCache.put("templates/ist-pagination/ist-pagination.html","<nav style=\"text-align: center; margin-top: 10px;\">\r\n	<span>Foram encontrados {{count}} registros. Mostrando registros de {{first}} até {{last}}.</span><br />\r\n	<ul class=\"pagination pagination-sm\" style=\"margin-top: 0px;\">\r\n		<li ng-class=\"{ disabled: ngModel.page == 1 }\"><a ng-href=\"{{goToPage( ngModel.page - 1 )}}\"><span aria-hidden=\"true\">&laquo;</span><span class=\"sr-only\">Previous</span></a></li>\r\n		<li ng-show=\"firstPage > 1\"><a ng-href=\"{{goToPage( 1 )}}\">1</a></li>\r\n		<li ng-show=\"firstPage > 1\" class=\"disabled\"><a href=\"\">...</a></li>\r\n		<li ng-repeat=\"page in pages\" ng-class=\"{ active: ngModel.page == page }\"><a ng-href=\"{{goToPage( page )}}\">{{page}}</a></li>\r\n		<li ng-show=\"lastPage < pageCount\" class=\"disabled\"><a href=\"\">...</a></li>\r\n		<li ng-show=\"lastPage < pageCount\"><a ng-href=\"{{goToPage( pageCount )}}\">{{pageCount}}</a></li>\r\n		<li ng-class=\"{ disabled: ngModel.page == pageCount }\"><a ng-href=\"{{goToPage( ngModel.page + 1 )}}\"><span aria-hidden=\"true\">&raquo;</span><span class=\"sr-only\">Next</span></a></li>\r\n	</ul>\r\n</nav>");}]);