var path = require ( 'path' );
var del = require ( 'del' );
var gulp = require ( 'gulp' );
var uglify = require ( 'gulp-uglify' );
var concat = require ( 'gulp-concat' );
var rename = require ( 'gulp-rename' );
var runSequence = require ( 'run-sequence' );
var angularTemplateCache = require( 'gulp-angular-templatecache' );

var distDir = './dist';

gulp.task ( 'build-clean', function () {
	return del ( path.join ( distDir, '*' ) );
} );

gulp.task ( 'build-copy-files', function () {
	return gulp.src ( [ 'src/infosites-commons.js' ] )
		.pipe ( gulp.dest ( distDir ) );
} );

gulp.task ( 'build-cache-templates', function () {
	return gulp.src( 'src/**/*.html' )
		//.pipe(minify and preprocess the template html here)
		.pipe( angularTemplateCache( 'infosites-commons-templates.js', { module: 'infosites-commons-templates', standalone: true } ) )
		.pipe( gulp.dest( distDir ) );
} );

gulp.task ( 'build-compress-js', function () {
	return gulp.src ( [ path.join( distDir, 'infosites-commons.js' ), path.join( distDir, 'infosites-commons-templates.js' ) ] )
		.pipe ( concat ( 'infosites-commons-all.js' ) )
		.pipe ( gulp.dest ( distDir ) )
		.pipe ( rename ( 'infosites-commons-all.min.js' ) )
		.pipe ( uglify () )
		.pipe ( gulp.dest ( distDir ) );
} );

gulp.task ( 'build', function () {
	return runSequence ( 'build-clean', 'build-copy-files', 'build-cache-templates', 'build-compress-js' );
} );
